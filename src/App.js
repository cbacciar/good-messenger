import React from 'react';
import {render} from 'react-dom';

import './Calcey.scss';
//import './App.css';

import Layout from './components/Layout/Layout';
import MessengerApp from './containers/MessengerApp/MessengerApp';

class App extends React.Component {
  render () {
    return (
      <div>
        <Layout>
          <MessengerApp />
        </Layout>
      </div>
    )
  }
}

render(<App/>, document.getElementById('root'));
