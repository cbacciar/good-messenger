import React, { Component } from 'react';

import Aux from '../../hoc/Auxiliary';
import Messenger from '../../components/Messenger/Messenger';

class MessengerApp extends Component {
  render () {
    return (
      <Aux>
        <Messenger />
      </Aux>
    );
  }
}

export default MessengerApp;
